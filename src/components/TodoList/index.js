import React, { Component } from 'react'
import TodoItem from '../TodoItem';

export default class TodoList extends Component {
    render() {
      let {deleteTodo, toggleCompleted, filter} = this.props;
      let filterKey = Object.keys(filter)[0];
      let filterCondition = filter[filterKey];
      
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos
              .filter(todo => todo[filterKey] === filterCondition)
              .map((todo, index) => (
                <TodoItem 
                  toggleCompleted={toggleCompleted} 
                  key={index}
                  id={index}
                  title={todo.title} 
                  completed={todo.completed}
                  deleteTodo={deleteTodo}
                />
            ))}
          </ul>
        </section>
      );
    }
  }
