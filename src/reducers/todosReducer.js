import initialState from './initialState';
import {
    ADD_TODO, 
    TOGGLE_COMPLETED, 
    DELETE_TODO,
    REMOVE_COMPLETED, 
    SET_TODOS
} from '../actions/actionTypes';

export default (state = initialState, action) => {
    switch(action.type) {
        case ADD_TODO:
            return Object.assign({}, state, {
                todos: [...state.todos, action.title]
            })

        case TOGGLE_COMPLETED:
            let todo = state.todos[action.id];
            todo.completed = !todo.completed;

            return Object.assign({}, state, {
                todos: [
                    ...state.todos.slice(0, action.id),
                    todo,
                    ...state.todos.slice(action.id + 1, state.todos.length)
                ]
            })

        case DELETE_TODO:
            return Object.assign({}, state, {
                todos: [
                    ...state.todos.slice(0, action.id),
                    ...state.todos.slice(action.id + 1, state.todos.length)
                ]
            })

        case REMOVE_COMPLETED:
            return Object.assign({}, state, {
                todos: state.todos.filter(todo => !todo.completed)
            })

        case SET_TODOS:
            return Object.assign({}, state, {
                todos: action.todos
            })
            
        default:
            return state;
    }
}