import React, { Component } from 'react'

const storeState = key => WrappedComponent => {
    class HOC extends Component {
        state = {
            localStorageAvailable: true
        }

        checkLocalStorageExists() {
            const testKey = 'test';

            try {
                localStorage.setItem(testKey, testKey);
                localStorage.removeItem(testKey);
                this.setState({localStorageAvailable: true})
            } catch(e) {
                this.setState({localStorageAvailable: false})
            }
        }

        load = key => {
            if (this.state.localStorageAvailable)
                return localStorage.getItem(key);

            return null;
        }

        remove = key => {
            if (this.state.localStorageAvailable)
                localStorage.removeItem(key);
        }

        componentDidMount() {
            this.checkLocalStorageExists();
        }
        
        render() {
            return (
                <WrappedComponent 
                    load={this.load}
                    remove={this.load}
                    {...this}
                />
            );
        }
    }

    return HOC;    
}

export default storeState;
